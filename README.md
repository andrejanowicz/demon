# About #

Demon is a **de**parture **mon**itor written in python. It connects to VRR's EFA interface and prints the results in a way to mimic the look of EVAG's public departure displays.

### How do I set up demon? ###

* install requests python module
* set executable bit
```
#!bash

pip(-3x) install requests
chmod +x demon.py

```
* Usage:

```
#!bash

./demon.py city station offset --platform 1,2,3,n --rows 5
```
* Example:

```
#!

./demon.py essen cäcilienstr 25 --platform 1,2 --rows 5

 106      Germaniaplatz           27 min
 101      Helenenstr.             30 min
 106      Germaniaplatz           37 min
 101      Helenenstr.             39 min
 106      Germaniaplatz           47 min

```